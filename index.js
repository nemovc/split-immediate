// include setImmediate shim
require('setimmediate');


const defaultOptions = {
  // time in ms before yielding
  // each loop of event que should run in under 16.6ms
  batchTime: 10,
  // context to run each cb in
  // falsy just means a direct call
  thisArg: false,
  // number of elements of 'data' to pass to 'cb' each time
  batchSize: 1,
  // whether the cb returns an output array or a single element
  // setting it to true will [...destructure] the return value onto the output array
  cbReturnsArray: false,
  // how often to call the progress callback
  // this is in number of times yielded
  progressFrequency: 30,
  // callback to use to update progress.
  // It is passed two parameters - last iteration, and total iterations
  progressCallback: undefined,
  // Whether to call after setImmediate or immediately
  delay: false,
  // if iterating an object, whether to keep input keys or map to flat array
  cbReturnsKeys: false,
};

/**
 * Perform complex calculations without locking up the browser.
 *
 * @param {Funtion} cb - Callback function executed on each data element
 * @param {Array|Object} data - Array or Object to iterate callback on
 * @param {Object} options - Object to configure behaviour of split-immediate
 *
 * @returns {Array|Object} - The result of running data through cb
 */

export function splitImmediate(cb, data, options = {}) {
  // check types
  if (typeof cb !== 'function') {
    throw Error('SplitImmediate: cb must be a function');
  }
  if (typeof data !== 'object') {
    throw Error('SplitImmediate: data must be an array or object');
  }
  if (typeof options !== 'object' && typeof options !== 'undefined') {
    throw Error('SplitImmediate: options must be an object');
  }
  if (typeof options.progressCallback !== 'undefined' && typeof options.progressCallback !== 'function') {
    throw Error('SplitImmediate: progressCallback must be a function');
  }

  // overwrite default options with local options as applicable
  const localOptions = Object.assign({}, defaultOptions, options || {});

  // set up closure'd values
  let resolve;
  let out;
  let outKeys = false;
  if (!Array.isArray(data) && localOptions.cbReturnsKeys) {
    out = {};
    outKeys = true;
  } else {
    out = [];
  }
  const retProm = new Promise((res) => {
    resolve = res;
  });
  let i = 0;
  let numYielded = 0;
  let batchStartTime = performance.now();

  // only bind thisArg once, if required
  const boundCB = localOptions.thisArg ? cb.bind(localOptions.thisArg) : cb;
  let handler;

  if (Array.isArray(data)) {
    handler = () => {
      for (; i < data.length; i += localOptions.batchSize) {
      // if enough time has passed that we should yield now
        if (performance.now() - batchStartTime > localOptions.batchTime) {
          batchStartTime = performance.now();
          setImmediate(handler);
          numYielded += 1;
          if (numYielded >= localOptions.progressFrequency) {
          // if enough iterations have gone past and we want to call the progressCallback
          // eslint-disable-next-line no-unused-expressions
            localOptions.progressCallback && localOptions.progressCallback(i, data.length);
            numYielded = 0;
          }
          return;
        }

        // return value of this iteration callback
        let cbRet;

        // call the CB with either one argument, or one argument of an array containing
        //  multiple input elements
        if (localOptions.batchSize === 1) {
          cbRet = boundCB(data[i], i);
        } else {
          cbRet = boundCB(data.slice(i, i + localOptions.batchSize), i);
        }

        // if it returns multiple 'output' elements, destructure them
        // otherwise, just push it
        if (typeof cbRet !== 'undefined') {
          if (localOptions.cbReturnsArray) {
            out.push(...cbRet);
          } else {
            out.push(cbRet);
          }
        }
      }
      // if the for loop gets to here, it's finished, so we can resolve with our output value
      // eslint-disable-next-line no-unused-expressions
      localOptions.progressCallback && localOptions.progressCallback(data.length, data.length);
      resolve(out);
    };
  } else {
    // data is an object
    // seperate handlers for speed at the cost of code duplication
    const keys = Object.keys(data);
    handler = () => {
      for (; i < keys.length; i += 1) {
        // if enough time has passed that we should yield now
        if (performance.now() - batchStartTime > localOptions.batchTime) {
          batchStartTime = performance.now();
          setImmediate(handler);
          numYielded += 1;
          if (numYielded >= localOptions.progressFrequency) {
            // if enough iterations have gone past and we want to call the progressCallback
            // eslint-disable-next-line no-unused-expressions
            localOptions.progressCallback && localOptions.progressCallback(i, keys.length);
            numYielded = 0;
          }
          return;
        }


        // call the CB with one argument
        const cbRet = boundCB(data[keys[i]], keys[i]);

        if (typeof cbRet !== 'undefined') {
          if (outKeys) {
            out[keys[i]] = cbRet;
          } else {
            if (localOptions.cbReturnsArray) {
              out.push(...cbRet);
            } else {
              out.push(cbRet);
            }
          }
        }
      }
      // if the for loop gets to here, it's finished, so we can resolve with our output value
      // eslint-disable-next-line no-unused-expressions
      localOptions.progressCallback && localOptions.progressCallback(keys.length, keys.length);
      resolve(out);
    };
  }


  // immediately call it and being execution
  if (localOptions.delay) {
    setImmediate(handler);
  } else {
    handler();
  }

  // return a (currently) unresolve promise
  return retProm;
}

export default splitImmediate;
