# split-immediate

Perform complex calculations without locking up the browser.

## Install
  ```sh
  npm install split-immediate
  ```
## Usage
  ```js
  const splitImmediate = require('split-immediate');

  function takesALongTime(element){
    // some computation that takes quite a long time here
    return value;
  }

  const data = [/* huge array here */];

  const output = await splitImmediate(takesALongTime, data);

  console.log(output);
  ```
  SplitImmediate can be used to ensure that the browser remains responsive while performing long-running tasks. It works best with problems that can be broken up into small iterations, but where the number of iterations is very great. 

## split-immediate function
  ```js
  splitImmediate(cb, data, options)
  ```
  * `cb` -  Callback function to run on each iteration. It recieves one or more elements of `data` as its first parameter, and the iteration key as the second parameter. The return value of this `cb` is added to the output (a return value of `undefined` is ignored).
  * `data` - Array or Object of data to operate on
  * `options` - Options object to modifiy the behaviour of splitImmediate. All values are optional.
    * `batchTime = 10` - How long (in ms) to run before allowing other tasks (e.g., UI) to be processed. Higher values will decrease overhead, but also decreased percieved responsiveness. The entire iteration of the event loop (including everything else the browser may be doing) should be less than 16ms. Also note that this is the *minimum* time a batch will execute for - if each execution of `cb` takes 5ms, a `batchTime` of `12` may result in each batch taking 15ms.
    * `batchSize = 1` - Number of elements of `data` that are passed to `cb` each run (see [batching](#batching) below)
    * `cbReturnsArray = false` - Whether the callback function returns an array of output objects or a single output object. (see [batching](#batching) below). Requires `data` to be an `Array` or `cbReturnsKeys` to be `false`
    * `cbReturnsKeys = false` - When iterating on objects, whether to preseve input keys. Requires `data` to be an `Object`. If set to false, the output is mapped to a flat array.
    * `thisArg = false` - If set to a non-falsy value, use this object as the `this` context for `cb`
    * `progressCallback: undefined` - Optional callback to run periodically to report back the state of the long-running task. It recieves two parameters `progressCallback(currentIteration, maxIterations)`. It will also be called when the task is completed.
    * `progressFrequency = 30` - How often to call `progressCallback`. Measured in number of times control is yielded (i.e., number of UI frames). 30 will update approximately every 0.5s. Lower values will provide more accurate feedback but increase overhead.

## Output
  `splitImmediate` returns a promise that is resolved once the entire task is completed. The resolved value is an array of the results of calling `cb` on each element of `data` - in a way, `splitImmediate` is a glorified `Array.map`.

## Batching
  `splitImmediate` provides support for `cb` functions that operate on different numbers of input elements through the `batchSize` option, and different numbers of output elements through the `cbReturnsArray` option.  
  
  If `batchSize === 1`, the `data` element is passed directly (i.e., `callback(data[i])`), otherwise an array is passed (i.e., `callback(data.slice(i, i + batchSize))`)

  For example, if your `data` consists of a flat array of `x`, `y` and `z` coordinates, which you want to turn into triangles (3 elements required for a vertex, and 3 verticies required for a triangle) you may use an `option` value of `{ batchSize: 9 }`

  Note that this option is ignored when iterating on an `Object`

  If `cbReturnsArray` is true, the return value of `cb` is destructured before being pushed onto the output array.

  For example, if your `cb` is the triangle generation callback from above, but instead of generating single triangle objects generates an array of 3 verticies, you may use an option value of `{ batchSize: 9, cbReturnsArray: true }`, which will ensure your output array is a flat map of the verticies you have generated.

  